import QtQuick 2.0

GridView {
    id: mainApp
    width: 600
    height: width
    anchors.fill: parent
    cellWidth: width/4
    cellHeight: cellWidth
    model: MyModel{}
    delegate: MyDelegate{}
    focus: true
    Keys.onUpPressed: model.moveTiles(0)
    Keys.onRightPressed: model.moveTiles(1)
    Keys.onDownPressed: model.moveTiles(2)
    Keys.onLeftPressed: model.moveTiles(3)
    Keys.onEscapePressed: model.newGame()
    move: Transition {
        SequentialAnimation{
            ParallelAnimation{
                NumberAnimation { properties: "x"; duration: 300; easing.type: Easing.Linear }
                NumberAnimation { properties: "y"; duration: 300; easing.type: Easing.Linear }
            }
            ScriptAction{ script: model.clearTable(); }
        }
    }
    displaced: Transition {
        SequentialAnimation{
            ParallelAnimation{
                NumberAnimation { properties: "x"; duration: 300; easing.type: Easing.Linear }
                NumberAnimation { properties: "y"; duration: 300; easing.type: Easing.Linear }
            }
            ScriptAction{ script: model.clearTable(); }
        }
    }
    remove: Transition {
        NumberAnimation { properties: "opacity"; from: 1.0; to: 0.0; duration: 300; }
    }
    Rectangle{
        id: scoreRectangle
        property int score: 0
        width: 60
        height: 30
        border.color: "#34B3A0"
        border.width: 3
        color: "transparent"
        anchors {right: parent.right; bottom: parent.bottom; rightMargin: 5; bottomMargin: 5 }
        Text {
            text: parent.score;
            font.pixelSize: 16
            color: "black"
            anchors.centerIn: parent
        }
    }
    Rectangle{
        id: victoryMessage
        width: 300
        height: 200
        visible: false
        focus: false
        color: "transparent"
        anchors.centerIn: parent
        Text{
            text: "You win! Congratulations!"
            font.pixelSize: 24
            color: "black"
            anchors.centerIn: parent
        }
        Keys.onSpacePressed: function(){
            visible = false
            mainApp.focus = true
            mainApp.model.newGame()
        }
    }
    function victory(){
        victoryMessage.visible = true
        victoryMessage.focus = true
    }
}


