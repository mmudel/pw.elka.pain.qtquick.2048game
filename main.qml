import QtQuick 2.6
import QtQuick.Window 2.2

Window {
    visible: true
    title: "2048"
    minimumHeight: 600
    minimumWidth: minimumHeight
    maximumWidth: height
    MyView { id: view }
}
