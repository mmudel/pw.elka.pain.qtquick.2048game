import QtQuick 2.0

ListModel {
    id: board

    property int finalScore: 2048
    property int score: 0
    property int oldScore: 0
    property bool created: false;

    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 2; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }
    ListElement {property int number: 0; property bool toRemove: 0; property int nextNumber: 0 }

    function getTile(col, row) {
        return board.get(col + row*4)
    }
    function putTile(col, row, tile){
        board.insert(col + row*4, tile)
    }
    function removeTile(col, row){
        board.remove(col + row*4)
    }
    function moveTile(oldCol, oldRow, newCol, newRow){
        board.move(oldCol + oldRow*4, newCol + newRow*4, 1)
    }
    function setTile(col, row, tile){
        board.set(col + row*4, tile)
    }

    function getFree() {
        var free = []
        for (var i = 0; i < 4; i++) {
            for (var j = 0; j < 4; j++) {
                if (getTile(i, j) && getTile(i, j).number === 0) {
                    free.push({colNum: i, rowNum: j})
                }
            }
        }
        return free[Math.floor(Math.random()*free.length)]
    }
    function createTile(){
        var newNumber = Math.random() > 0.7 ? 4 : 2
        var free = getFree()
        removeTile(free.colNum, free.rowNum)
        putTile(free.colNum, free.rowNum, { "number": newNumber, "toRemove": 0 })
    }

    function moveTiles(direction){
        created = false
        var count = 0
        var hasMoved = false
        var hasCleared = false
        var k = 0
//        if(moving == true){
//            return
//        }
//        moving = true
        oldScore = score
        for(var i=0; i<4; i++){
            for(var j=0; j<4; j++){

            }
        }

        /* North */
        if(direction === 0){
            for(var i=0; i<4; i++){
                for(var j=0; j<4; j++){
                    if(getTile(i, j).number !== 0){
                        for(k = j-1; k>=0; k--){
                            if(getTile(i, k).number === 0 || getTile(i, k).toRemove === 1){
                                count++;
                            }
                            else{
                                if(getTile(i, k).number === getTile(i, j).number){
                                    board.getTile(i, k).nextNumber = board.getTile(i, k).number*2
                                    board.setTile(i, j, { "toRemove": 1 })
                                    score += board.getTile(i, k).number
                                    hasCleared = true
                                }
                                break;
                            }
                        }
                    }
                    if(count > 0){
                        board.removeTile(i, j-count)
                        board.moveTile(i-1, j, i, j-count)
                        board.putTile(i, j, { "number": 0, "toRemove": 0 })
                        hasMoved = true;
                    }
                    count = 0;
                }
            }
        }
        /* East */
        else if(direction === 1){
            for(var i=3; i>=0; i--){
                for(var j=0; j<4; j++){
                    if(getTile(i, j).number !== 0){
                        for(k = i+1; k<4; k++){
                            if(getTile(k, j).number === 0 || getTile(k, j).toRemove === 1){
                                count++;
                            }
                            else{
                                if(getTile(k, j).number === getTile(i, j).number){
                                    board.getTile(k, j).nextNumber = board.getTile(k, j).number*2
                                    board.setTile(i, j, { "toRemove": 1 })
                                    score += board.getTile(k, j).number
                                    hasCleared = true
                                }
                                break;
                            }
                        }
                    }
                    if(count > 0){
                        board.removeTile(i+count, j)
                        board.moveTile(i, j, i+count-1, j)
                        board.putTile(i, j, { "number": 0, "toRemove": 0 })
                        hasMoved = true;
                    }
                    count = 0;
                }
            }
        }
        /* South */
        else if(direction === 2){
            for(var i=0; i<4; i++){
                for(var j=3; j>=0; j--){
                    if(getTile(i, j).number !== 0){
                        for(k = j+1; k<4; k++){
                            if(getTile(i, k).number === 0 || getTile(i, k).toRemove === 1){
                                count++;
                            }
                            else{
                                if(getTile(i, k).number === getTile(i, j).number){
                                    board.getTile(i, k).nextNumber = board.getTile(i, k).number*2
                                    board.setTile(i, j, { "toRemove": 1 })
                                    score += board.getTile(i, k).number
                                    hasCleared = true
                                }
                                break;
                            }
                        }
                    }
                    if(count > 0){
                        board.removeTile(i, j+count)
                        board.moveTile(i, j, i-1, j+count)
                        board.putTile(i, j, { "number": 0, "toRemove": 0 })
                        hasMoved = true;
                    }
                    count = 0;
                }
            }
        }
        /* West */
        else if(direction === 3){
            for(var i=0; i<4; i++){
                for(var j=0; j<4; j++){
                    if(getTile(i, j).number !== 0){
                        for(k = i-1; k>=0; k--){
                            if(getTile(k, j).number === 0 || getTile(k, j).toRemove === 1){
                                count++;
                            }
                            else{
                                if(getTile(k, j).number === getTile(i, j).number){
                                    board.getTile(k, j).nextNumber = board.getTile(k, j).number*2
                                    board.setTile(i, j, { "toRemove": 1 })
                                    score += board.getTile(k, j).number
                                    hasCleared = true
                                }
                                break;
                            }
                        }
                    }
                    if(count > 0){
                        board.removeTile(i-count, j)
                        board.moveTile(i-1, j, i-count, j)
                        board.putTile(i, j, { "number": 0, "toRemove": 0 })
                        hasMoved = true;
                    }
                    count = 0;
                }
            }
        }
        if(hasMoved === false && hasCleared === true){
            board.clearTable()
            board.createTile()
            created = true;
        }
    }

    function clearTable(){
        for(var i=0; i<4; i++){
            for(var j=0; j<4; j++){
                if(board.getTile(i, j) && board.getTile(i, j).nextNumber !== 0){
                    board.getTile(i, j).number = board.getTile(i, j).nextNumber
                    if(board.getTile(i, j).number === finalScore){
                     mainApp.victory()
                    }
                    board.getTile(i, j).nextNumber = 0
                }
                if(board.getTile(i, j) && board.getTile(i, j).toRemove === 1){
                    board.removeTile(i, j)
                    board.putTile(i, j, { "number": 0, "toRemove": 0 })
                }
            }
        }
        if(created === false){
            board.createTile()
            created = true;
        }
    }
    function newGame(){
        for(var i=0; i<4; i++){
            for(var j=0; j<4; j++){
                board.removeTile(i, j)
                if(i==2 && j==1)
                    board.putTile(i, j, {"number": 2, "toRemove": 0})
                else
                    board.putTile(i, j, {"number": 0, "toRemove": 0})
            }
        }
        board.score = 0
    }
    onScoreChanged: scoreRectangle.score = board.score
}
