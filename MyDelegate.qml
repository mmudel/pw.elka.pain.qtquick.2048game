import QtQuick 2.0

Component {
    id: tileDelegate
    Rectangle{
        id: tile
        width: mainApp.width/4-2
        height: width
        border.color: number == 0 ? "transparent" : "silver"
        border.width: 2
        property int textNumber: number
        //anchors {left: parent.left; top: parent.top; leftMargin: xVal; topMargin: yVal }
        color: number ==    2 ? "yellow" :
               number ==    4 ? "red" :
               number ==    8 ? "blue" :
               number ==   16 ? "green" :
               number ==   32 ? "darkRed" :
               number ==   64 ? "darkCyan" :
               number ==  128 ? "darkBlue" :
               number ==  256 ? "darkGreen" :
               number ==  512 ? "darkYellow" :
               number == 1024 ? "cyan" :
               number == 2048 ? "magenta" : "transparent"
        Behavior on textNumber{
            NumberAnimation{
                duration: 300
            }
        }
        Text {
            text: textNumber;
            font.pixelSize: 24
            color: number == 0 ? "transparent" : "black"
            anchors.centerIn: parent
        }
    }
}
